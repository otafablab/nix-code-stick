{...}: {
  programs.home-manager.enable = true;

  home = {
    sessionVariables.NIXPKGS_ALLOW_UNFREE = 1;
    stateVersion = "23.11";
  };

  xdg = {
    mimeApps.enable = true;
    # Disable useless user dirs
    userDirs = {
      enable = true;
      createDirectories = true;
      desktop = null;
      publicShare = null;
      templates = null;
    };
  };
}
