{...}: {
  fileSystems = {
    "/" = {
      device = "none";
      fsType = "tmpfs";
      # NOTE: nr_inodes=0 is not encouraged in kernel docs:
      # "It is generally unwise to mount with such options, since it allows any
      # user with write access to use up all the memory on the machine; but
      # enhances the scalability of that instance in a system with many cpus
      # making intensive use of it."
      options = ["defaults" "size=4G" "nr_inodes=0" "mode=755"];
    };
    "/home" = {
      device = "/dev/disk/by-label/code-stick-home";
      fsType = "ext4";
    };
  };

  # zram
  swapDevices = [];
  zramSwap.enable = true;

  boot.loader = {
    efi.canTouchEfiVariables = true;
    grub = {
      efiSupport = true;
      device = "nodev";
    };
  };
}
