{
  description = "Nix code ISO";

  outputs = {
    self,
    nixpkgs,
    home-manager,
    flake-parts,
    devenv,
    nixos-generators,
  } @ inputs:
    flake-parts.lib.mkFlake {inherit inputs;} {
      flake = {
        nixosConfigurations.code-stick = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            home-manager.nixosModule
            nixos-generators.nixosModules.all-formats
            ./system
            {
              home-manager = {
                useGlobalPkgs = true;
                useUserPackages = true;
                users.oppilas = import ./user;
              };
            }
          ];
          specialArgs = {inherit inputs;};
        };
      };

      systems = ["x86_64-linux"];

      imports = [
        inputs.devenv.flakeModule
      ];

      perSystem = {pkgs, ...}: {
        devenv.shells.default = {
          pre-commit = {
            hooks = {
              alejandra.enable = true;
              deadnix.enable = true;
              nil.enable = true;
              statix.enable = true;
            };
            settings = {
              deadnix = {
                edit = true;
                hidden = true;
              };
            };
          };
          # TODO: Remove when https://github.com/cachix/devenv/issues/760 is fixed
          containers = pkgs.lib.mkForce {};
        };
      };
    };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-parts.url = "github:hercules-ci/flake-parts";
    devenv = {
      url = "github:cachix/devenv";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
}
